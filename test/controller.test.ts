import controller from '../source/controllers/lucky';
test('Dummy Test',()=>{
    // AAA Unit Testing 
    // Arrange 
    let  x = 1;
    let y = 2;
    // Act - the call for the function under test
    let sum  = 1 + 2;
    // Assert - compare restul of the function under test with expected result
    expect(sum).toBe(3)
})


test('random lucky number is less than or equal max number', ()=>{
    // Arrange
    let maxNumber: number = 10;

    // Act
    let luckyNumber: number = controller.getRandomLuckyNumber(10);

    // Assert
    expect(luckyNumber).toBeLessThanOrEqual(maxNumber)
})

test('random lucky number is not negative',()=>{
    let maxNumber: number = 10;

    // Act
    let luckyNumber: number = controller.getRandomLuckyNumber(10);

    // Assert
    expect(luckyNumber).toBeGreaterThan(0)
})

test('random lucky number is not greater than max number ', ()=>{
    let maxNumber: number = 10;

    // Act
    let luckyNumber: number = controller.getRandomLuckyNumber(10);

    // Assert
    expect(luckyNumber).not.toBeGreaterThan(10)
})