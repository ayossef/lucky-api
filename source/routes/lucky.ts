import express from 'express';
import controller from '../controllers/lucky';

const router = express.Router();

router.get('/luckyCustomer', controller.getLuckyCustomer);
router.get('/updateToken', controller.updateToken)
export = router