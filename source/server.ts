// import require packages
import express, {Express} from 'express';
import routes from './routes/lucky';
import http from 'http';
import morgan from 'morgan';

const server: Express = express();

// Use the packegs
server.use(morgan('dev'));
server.use(express.urlencoded())
server.use(express.json())
// add the routes
server.use('/', routes);


const httpServer = http.createServer(server);
httpServer.listen(8080, ()=> {console.log('Server is up and running .. ')})
