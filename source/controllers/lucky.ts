import {Request, Response, NextFunction} from 'express';
import axios, { AxiosResponse } from 'axios';
interface Customer{
    id: Number,
    firstname: String,
    lastname: String, 
    email: String
}
var apiToken: String = 'eyJraWQiOiIxIiwiYWxnIjoiSFMyNTYifQ.eyJ1aWQiOjEsInV0eXBpZCI6MiwiaWF0IjoxNjY1NDc4MDI0LCJleHAiOjE2NjU0ODE2MjR9.LOAvRipSdsNu4SWn9cuwKq2R1DvrxbecTG0YU-X4o4A'
const getLuckyCustomer = async (req:Request, res: Response, next:NextFunction)=>{
const url = 'http://localhost/rest/all/V1/customers/1';
const config = {
    url:url,
    method:'get',
    headers:{Authorization: `Bearer ${apiToken}`}
}
    let result: AxiosResponse  = await axios.get(url, config);
    let luckyCustomer: Customer = result.data;

    return res.status(200).json({
        message: {id:luckyCustomer.id, email:luckyCustomer.email, fn:luckyCustomer.firstname, ln:luckyCustomer.lastname}
    })
}

const updateToken = async (req:Request, res:Response, next:NextFunction)=>{
    const url = 'http://localhost/rest/all/V1/integration/admin/token';
    const config = {
        method:'post',
        url: url
    }
    const body = {username:"user", password:"bitnami1"};
    let result = await axios.post(url, body, config);
    let token: String = result.data;
    apiToken = token;
    console.log(token);
    return res.status(200).json({
        message: 'Token Updated'
    });
}
const getRandomLuckyNumber = (max: number)=>{
    let min: number = 0;
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random()*(max - min +1)) + min;
}

export default {getLuckyCustomer, updateToken, getRandomLuckyNumber}