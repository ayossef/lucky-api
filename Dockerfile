FROM node:16
WORKDIR /usr/app
COPY source ./source
COPY package.json ./
COPY tsconfig.json ./
RUN pwd
RUN ls -al
RUN npm install
EXPOSE 6060
CMD ["npm", "run", "dev"]